var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', () => {
  res.render('index', {name: 'Sradha', Company: 'RishabhSoft', task: 'R&D'});
})

module.exports = router;
